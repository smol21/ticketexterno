import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const state = {
  sidebarShow: 'responsive',
  sidebarMinimize: false,
  ticketsInStore: JSON.parse(localStorage.getItem('ticketsInStore') || '[]'),
  ticketsData: JSON.parse(localStorage.getItem('ticketsData') || 'null'),
  priceAll: {},
}

const mutations = {
  toggleSidebarDesktop (state) {
    const sidebarOpened = [true, 'responsive'].includes(state.sidebarShow)
    state.sidebarShow = sidebarOpened ? false : 'responsive'
  },
  toggleSidebarMobile (state) {
    const sidebarClosed = [false, 'responsive'].includes(state.sidebarShow)
    state.sidebarShow = sidebarClosed ? true : 'responsive'
  },
  set (state, [variable, value, ls]) {
    if (ls) {
      if (value.length) localStorage.setItem(variable, JSON.stringify(value));
      else localStorage.removeItem(variable);
    }
    state[variable] = value
  }
}

const getters = {
  ticketsInStore (state) {
    return state.ticketsInStore;
  },
  ticketsData (state) {
    return state.ticketsData;
  },
  priceAll (state) {
    return state.priceAll;
  },
}


export default new Vuex.Store({
  state,
  mutations,
  getters
})