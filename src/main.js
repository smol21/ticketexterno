import 'core-js/stable';
import Vue from 'vue';
import VueSweetalert2 from 'vue-sweetalert2';
import App from './App';
import { iconsSet as icons } from './assets/icons/icons.js';
import CoreuiVue from '@coreui/vue';
import store from './store';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import 'sweetalert2/src/sweetalert2.scss';
import Vuelidate from 'vuelidate';
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import VueToastr2 from 'vue-toastr-2';
import 'vue-toastr-2/dist/vue-toastr-2.min.css';
import VueFormWizard from 'vue-form-wizard';
import 'vue-form-wizard/dist/vue-form-wizard.min.css';
import VueFlicking from '@egjs/vue-flicking';
import '@egjs/vue-flicking/dist/flicking.css';
import VueScratchable from 'vue-scratchable';
import VueDatePicker from '@mathieustan/vue-datepicker';
import '@mathieustan/vue-datepicker/dist/vue-datepicker.min.css';
import SmartTable from 'vuejs-smart-table';
import mitt from 'mitt';
import VueSocialSharing from 'vue-social-sharing'

const emitter = mitt();
window.toastr = require('toastr');
const URLactual = window.location /* 'aHR0cDovL2Nhc2Fmb3J0dW5hLmNvbS52ZS8wMTkvMDAzOTA4LzE2ODU1MzcxMTkvMDk5' */ /*'aHR0cDovL2Nhc2Fmb3J0dW5hLmNvbS52ZS8wMTcvMDAzMjMxLzE2ODM1NTQ5ODgvMDk5'*/;
const pathName = URLactual.pathname;
const primerSplit = pathName.split('/');
  // const path = window.atob(primerSplit[1]);
  // const datosUrl = path.split('/');
  const codNumSotero = primerSplit[1] ? primerSplit[1] : '';
  const codNumFactura = primerSplit[2] ? primerSplit[2] : '';
  const fechaSorteo = window.atob(primerSplit[3]) ? primerSplit[3] : '';
  const codDistribuidor = primerSplit[4] ? primerSplit[4] : '';

  localStorage.setItem('codNumSotero',codNumSotero);
  localStorage.setItem('codNumFactura',codNumFactura);
  localStorage.setItem('fechaSorteo',fechaSorteo);
  localStorage.setItem('codDistribuidor',codDistribuidor);




window.$ = window.jQuery = require('jquery');
// Vue.prototype.$apiGeneral = urlApi + 'serviceDealer/ServicioDistribuidor.svc/ServicioDistribuidor/';

// export const API_GENERAL = API_URL_BACKEND + 'serviceDealer/ServicioDistribuidor.svc/ServicioDistribuidor/';
Vue.config.performance = true;
Vue.use(CoreuiVue);
Vue.use(VueSweetalert2);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(Vuelidate);
Vue.use(VueToastr2);
Vue.use(VueFormWizard);
Vue.use(VueFlicking);
Vue.use(SmartTable);
Vue.use(VueSocialSharing);
Vue.prototype.emitter = emitter;

Vue.use(VueDatePicker, {
  lang: 'es',
});
Vue.component('vue-scratchable', VueScratchable);
Vue.component('loading-overlay', Loading);
Vue.prototype.$bus = new Vue();
new Vue({
  el: '#app',
  store,
  icons,
  template: '<App/>',
  components: {
    App,
  },
});
