import GeneralPurpose from "./generalPurpose";

   function getTicketData (data = null){
    const urlColor = data.urlSorteo;
    const urlInstantaneo = require('@/assets/' + urlColor + '_instantaneo.png');
    const sectionObj = (case_, keys, i, key) => {
      const decodedDataKey = window.atob(data[key]);
      let obj = {};
      switch (case_) {
        case 1: 
          obj = { 
            [`pm${i + 1}1`]: decodedDataKey.slice(0,3), 
            [`pm${i + 1}2`]: decodedDataKey.slice(3) 
          }; 
          break;
        // case 2: 
        //   obj = { 
        //     [`tt${i + 1}`]: decodedDataKey 
        //   }; 
        //   break;
        // case 3: 
        //   obj = { 
        //     [`sd${i + 1}1`]: decodedDataKey.slice(0,3), 
        //     [`sd${i + 1}2`]: decodedDataKey.slice(3) 
        //   }; 
        //   break;
        case 4: 
          const ind = !i ? [0, 3] : i < 2 ? [3, -3] : [-3]; 
          obj = { 
            [`i${i + 1}1`]: decodedDataKey.slice(...ind).slice(0,1), 
            [`i${i + 1}2`]: decodedDataKey.slice(...ind).slice(1,-1), 
            [`i${i + 1}3`]: decodedDataKey.slice(...ind).slice(-1) 
          }; 
          break;
      }
      return { ...keys, ...obj };
    };
    const fecha  = data['fechaSorteo']
    // Obtener el día, mes y año
    const dia = String(fecha.getDate()).padStart(2, '0'); // Asegura que tenga dos dígitos
    const mes = String(fecha.getMonth() + 1).padStart(2, '0'); // Los meses son 0-indexados
    const año = fecha.getFullYear();
    const fechaFormateada = `${dia}/${mes}/${año}`;
    const sorteoDesEncrip = window.atob(data['codigoSorteo']);
    return {  
      image: require('@/assets/' + data['urlSorteo'] + '_abierto.jpg'),
      images: [
        !(+data['estatusParMil']) ? require("@/assets/cartonImg1.png") : null,
        // !(+data['estatusTripleTer']) ? require("@/assets/cartonImg2.png") : null,
        // !(+data['estatusSuperDup']) ? require("@/assets/cartonImg3.png") : null,
        !(+data['estatusInstantaneo']) ? urlInstantaneo : null,
      ],
      ticket: {
        borderColor: data['colorBorde'] || '#998f87',
        code: window.atob(data['tripleAbierto']),
        numeroFactura: data['numeroFactura'],
        numeroSorteo: sorteoDesEncrip,
        serialTicket: data['serialTicket'],
        codOperacion: data['codigoOperacion'],
        fechaSort: fechaFormateada ,
        scratches: [
          // PAR MILLONARIO 
          ['parMillonarioA', 'parMillonarioB'].reduce( (keys, key, i) => sectionObj(1, keys, i, key), {} ),
          // TRIPLE TERMINAL
          // ['tripleTerminalA', 'tripleTerminalB', 'tripleTerminalC'].reduce( (keys, key, i) => sectionObj(2, keys, i, key), {} ),
          // SUPER DUPLETA
          // ['superDupleta1', 'superDupleta2', 'superDupleta3'].reduce( (keys, key, i) => sectionObj(3, keys, i, key), {} ),
          // INSTANTANEO
          Array(3).fill('instantaneo').reduce( (keys, key, i) => sectionObj(4, keys, i, key), {} ),
        ]
      }
    };
  }
  
  export default {
    mixins: [GeneralPurpose],
    methods: {
      getTicketData,
    },
    computed: {

    }
  }