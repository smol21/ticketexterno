import XLSX from "xlsx";
import axios from 'axios';

const NumWhitCommas = (number) => {
  return number.toString().replace(".", ",");
};

const DateFormater = {
  toNormalDate(date, separator) {
    let newDay = new Date(date),
      day = ("0" + newDay.getDate()).slice(-2),
      month = ("0" + (newDay.getMonth() + 1)).slice(-2),
      year = newDay.getFullYear();

    separator = separator ? separator : "/";

    return `${day}${separator}${month}${separator}${year}`;
  },
  toLongDate(date) {
    let meses = new Array(
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre"
    );

    return `${date.getDate()} de ${
      meses[date.getMonth()]
    } de ${date.getFullYear()}`;
  },
  toInvertDate(date, separator) {
    let newDay = new Date(date),
      day = ("0" + newDay.getDate()).slice(-2),
      month = ("0" + (newDay.getMonth() + 1)).slice(-2),
      year = newDay.getFullYear();

    separator = separator ? separator : "-";

    return `${year}${separator}${month}${separator}${day}`;
  },

  DateFormaterEpoch(date, withSlash = false, enType = false) {
    const d = new Date(date * 1000);
    const date_ = d.getDate(), month_ = d.getMonth() + 1, year_ = d.getFullYear();
    const dateVal = `${date_}`.length > 1 ? date_ : `0${date_}`;
    const datestring = 
      (enType ? year_ : dateVal) + `${withSlash ? '/' : '-'}` + 
      (`${month_}`.length > 1 ? month_ : `0${month_}`) + `${withSlash ? '/' : '-'}` + (enType ? dateVal : year_);
    return `${datestring}`;
  },
};
const GenerateExcel = (jsonArray = [], name) => {
  if (Array.isArray(jsonArray)) {
    let fileName = name ? name : "";
    let worksheet = XLSX.utils.json_to_sheet(jsonArray);
    let workbook = XLSX.utils.book_new();
    let today = DateFormater.toNormalDate(new Date(), "-");
    let time = new Date().getTime();

    XLSX.utils.book_append_sheet(workbook, worksheet, `${fileName}`);
    XLSX.writeFile(workbook, `${fileName}_${today}_${time}.xlsx`);
  } else {
    
  }
};

function GenerateImgPlaceholder(
  text = "N/A",
  width = 200,
  height = 200,
  colorTxt = "#4f5d73",
  colorBg = "#ebedef"
) {
  let canvas = document.createElement("canvas");
  let context = canvas.getContext("2d");
  canvas.width = height >= 100 ? height : 100;
  canvas.height = width = 100 ? width : 100;

  context.fillStyle = colorBg;
  context.fill();

  context.font = canvas.height / 4 + "px Arial";
  context.fillStyle = colorTxt;

  context.textAlign = "center";
  context.textBaseline = "middle";
  context.fillText(text, canvas.width / 2, canvas.height / 2);

  return canvas.toDataURL();
}

function GenerateSvgPlaceholder(options = {}) {
  let defaults = {
    width: 200,
    height: 150,
    bgColor: "#ebedef",
    textColor: "#4f5d73",
    fontFamily: "sans-serif",
    fontSize: 30,
    dy: 10.5,
    fontWeight: "bold",
    text: "NO IMG",
  };
  options = { ...defaults, ...options };

  let svg = `<svg xmlns="http://www.w3.org/2000/svg" width="${options.width}" height="${options.height}" viewBox="0 0 ${options.width} ${options.height}">
  <rect fill="${options.bgColor}" width="${options.width}" height="${options.height}"/>
  <text fill="${options.textColor}" font-family="${options.fontFamily}" font-size="${options.fontSize}" dy="${options.dy}" font-weight="${options.fontWeight}" x="50%" y="50%" text-anchor="middle">${options.text}</text>
</svg>`;

  const cleaned = svg
    .replace(/[\t\n\r]/gim, "") // Strip newlines and tabs
    .replace(/\s\s+/g, " ") // Condense multiple spaces
    .replace(/'/gim, "\\i"); // Normalize quotes

  const encoded = encodeURIComponent(cleaned)
    .replace(/\(/g, "%28") // Encode brackets
    .replace(/\)/g, "%29");

  return `data:image/svg+xml;charset=UTF-8,${encoded}`;
}

const tableTextHelpers = {
  tableFilterText: {
    label: "Filtrar:",
    placeholder: "INGRESE BUSQUEDA...",
  },
  itemsPerPageText: {
    label: "Registros por Pagina:",
  },
  noItemsViewText: {
    noResults: "No se han encontrado resultados",
    noItems: "No hay registros disponibles",
  },
};

const imgInputMsgs = {
  upload: "<p>Tu dispositivo no soporta la subida de archivos.</p>", // HTML allowed
  drag: "Click aqui para seleccionar un <br>FOTO", // HTML allowed
  tap: "Toque para seleccionar un FOTO", // HTML allowed
  change: "Cambiar Foto", // Text only
  remove: "Eliminar Foto", // Text only
  select: "Seleccionar un Foto", // Text only
  selected: "<p>Foto seleccionado!</p>", // HTML allowed
  fileSize: "El tamaño del archivo excede el limite permitido.", // Text only
  fileType: "El tipo de archivo no esta permitido.", // Text only
  aspect: "Landscape/Portrait", // Text only
};

function validarExtension(datos) {
  let ruta = datos.name;

  if (!/\.(jpg|svg|jpeg|png|bmp|gif)$/i.test(datos.name)) {
    return false;
  } else {
    return true;
  }
}

function validURL(str) {
  var pattern = new RegExp(
    "^(https?:\\/\\/)?" + // protocol
      "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
      "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
      "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
      "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
      "(\\#[-a-z\\d_]*)?$",
    "i"
  ); // fragment locator
  return !!pattern.test(str);
}

function slugify(text) {
  const a = "àáäâèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;";
  const b = "aaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------";
  const p = new RegExp(a.split("").join("|"), "g");

  return text
    .toString()
    .toLowerCase()
    .replace(/\s+/g, "-") // Replace spaces with -
    .replace(p, (c) => b.charAt(a.indexOf(c))) // Replace special chars
    .replace(/&/g, "-and-") // Replace & with 'and'
    .replace(/[^\w\-]+/g, "") // Remove all non-word chars
    .replace(/\-\-+/g, "-") // Replace multiple - with single -
    .replace(/^-+/, "") // Trim - from start of text
    .replace(/-+$/, ""); // Trim - from end of text
}

function menuFormatter (datos_) {
  const menu = datos_.menuList.reduce((menu, item) => {
    if (+item.fatherCode == 0 && !menu[+item.menuCode]) 
      menu[+item.menuCode] = {
        ...item,
        menuCode: +item.menuCode,
        fatherCode: +item.fatherCode, 
        children: [],
      };
    else if (+item.fatherCode == 0 && menu[+item.menuCode]) 
      menu[+item.menuCode] = {
        ...menu[+item.menuCode],
        ...item,
        menuCode: +item.menuCode,
        fatherCode: +item.fatherCode,
      };
    else if (menu[+item.fatherCode]) 
      menu[+item.fatherCode].children.push({
        ...item,
        menuCode: +item.menuCode,
        fatherCode: +item.fatherCode,
      });
    else 
      menu[+item.fatherCode] = {
        children: [{
          ...item,
          menuCode: +item.menuCode,
          fatherCode: +item.fatherCode,
        }],
      }
    return menu;
  }, {});

  return menu;
}

async function axiosRequest({self = null, method, url, data = null, handleError = false, headers = null, inner = false}) {
  try {
      const resp = await axios({
          method,
          url,
          ...( data ? { data } : {}),
          ...( headers ? { headers } : {}),
        });/* axios[method](url, { ...data }); */
      return resp;
  } catch (err) {
      console.error(err);
      if (handleError) {
        if (self.loading) self.loading = false;
        if (self.Loading) self.Loading = false;
        
        const msg = err?.response?.data?.message?.description || err?.responseJSON?.message?.description || err?.responseJSON?.mensaje?.description || err?.response?.statusText || '';
        if (msg) {
          self.fireToastMsg(msg);
          const fechaVencimiento = localStorage.getItem("fecha");
          const fechaActual = new Date().toISOString();

          if (fechaActual > fechaVencimiento) { 
            location.reload();
          }
        }  
        else self.globalError(true);
        if (self[inner ? 'login' : 'type'] === 'login' && (err.response?.data?.message?.code === "032" || err.response?.data?.message?.code === "052")) {
            setTimeout(() => {
              self[inner ? 'setType' : 'changeTypeMeth']('changepassword');
            }, (self.getMsgSeconds(msg) / 2));
        }
      }
      if (err.response?.data?.message?.code === "008" && self.clearSession) self.clearSession();
      return  false;
  }
}

function dataFromGeocoding(json) {

  const {
      sublocality,
      route,
      state,
      municipality,
      city,
      parish
  } = json.data.results.reduceRight((addressData, currAddrComp) => {
      const reducedAddrs = currAddrComp.address_components.reduce((reducedData, currAddrs) => {
         
          const setCond = (key) => {
              let str = currAddrs.long_name || currAddrs.short_name;
              const pattern = new RegExp(`${key} `, 'gi');
              if (pattern.test(str)) str = str.replace(pattern, '');
              reducedData[key] = str;
          };
          const conds = [
              ['administrative_area_level_1','estado'],
              ['administrative_area_level_2','municipio'],
              ['locality','ciudad'],
              ['administrative_area_level_3','parroquia'],
              ['sublocality'],
              ['route']
          ];
          for (let j = 0; j < conds.length; j++) if (currAddrs.types.includes(conds[j][0])) { setCond(conds[j][conds[j].length - 1]); break; }

          return reducedData;
      }, {});

      addressData = {
          ...addressData,
          ...(reducedAddrs.estado ? { state: reducedAddrs.estado } : {}),
          ...(reducedAddrs.municipio ? { municipality: reducedAddrs.municipio } : {}),
          ...(reducedAddrs.ciudad ? { city: reducedAddrs.ciudad } : {}),
          ...(reducedAddrs.parroquia ? { parish: reducedAddrs.parroquia } : {}),
          ...(reducedAddrs.sublocality ? { sublocality: reducedAddrs.sublocality } : {}),
          ...(reducedAddrs.route ? { route: reducedAddrs.route } : {}),
      };

      return addressData;
  }, {});

  return {
      sublocality,
      route,
      state,
      municipality,
      city,
      parish
  }
}

function timePassed (ticketsData) {
  const now = new Date().getTime();
  const fechaReserva = ticketsData.fechaReserva * 1000;
  const seconds = (now - fechaReserva) / 1000; // seconds
  // const minutes = seconds / 60;
  // const hours = minutes / 60;
  // const days = hours / 24;
  // const years = days / 365;

  return /* hours */seconds;
}

function controlOverCart (tickets_, ticketsData,  all = false) {
  const {newTickets, removedTickets} = tickets_.reduce((tickets, ticket) => {
      const /* hours */seconds = timePassed(ticketsData);
      // Tiempo que dura el ticket en el carrito
      if (seconds <= ticketsData.tiempoReserva) tickets.newTickets.push(ticket);
      else tickets.removedTickets.push(ticket);

      return tickets;
  }, {
    newTickets: [],
    removedTickets: [],
  });

  if (all) return {
    newTickets,
    removedTickets
  };
  else return newTickets;
}

export {
  NumWhitCommas,
  DateFormater,
  tableTextHelpers,
  imgInputMsgs,
  GenerateExcel,
  validarExtension,
  GenerateImgPlaceholder,
  GenerateSvgPlaceholder,
  validURL,
  slugify,
  menuFormatter,
  axiosRequest,
  dataFromGeocoding,
  controlOverCart,
};
