module.exports = {
  root: true,
  env: {
    node: true,
    es6: true,
    browser: true,
  },

  extends: ['plugin:vue/essential', 'eslint:recommended'],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    quotes: ['error', 'single'],
    'no-const-assign': 'error',
    'no-dupe-args': 'error',
    'no-duplicate-imports': 'error',
    'no-unreachable': 'error',
    'no-use-before-define': 'error',
    'block-scoped-var': 'error',
    'max-len': 'off',
    'no-empty': 'error',
    'no-empty-function': 'error',
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
};
