import { sidebarCssClasses } from '@/shared/classes'
import toggleClasses from '@/shared/toggle-classes'

test('toggleClasses', () => {
  const cssClass = `sidebar-show`
  expect(toggleClasses.constructor === Function).toBe(true)
  toggleClasses(cssClass, sidebarCssClasses )

  expect(document.body.classList).toMatchObject({ '0': 'sidebar-show' })
  toggleClasses("sidebar-xl-show", sidebarCssClasses )
 
  toggleClasses("sidebar-lg-show", sidebarCssClasses )
  
  toggleClasses("sidebar-md-show", sidebarCssClasses )
  expect(document.body.classList).toMatchObject({ '0': 'sidebar-xl-show', '1': 'sidebar-lg-show', '2': 'sidebar-md-show' })

  toggleClasses("sidebar-xl-show", sidebarCssClasses )
  
  toggleClasses(cssClass, sidebarCssClasses )
  expect(document.body.classList).toMatchObject({ '0': 'sidebar-show' })
  
});
